# Change Log

All notable changes to the "yich" extension will be documented in this file.

## [0.0.2]e
- Added snippet yalaExtendClass, which preps a script include to be extended.

## [0.0.1]

- Initial release
- Includes snippets for
    - SystemLoggerHelper
        - yalaDebug
        - yalaInfo
        - yalaWarn
        - yalaReportException
    - Working In Script Includes
        - yalaClassFunc
- Once extension is activated, type "yala" in a JS file and see all the snippets that come up!