const sv = require('semver');
const path = require('path');
const fs = require('fs').promises;

const release = process.argv.slice(2)[0];

let updatePackageJSON = async function () {
    let packagePath = path.join('package.json');
    var packageJSON = await fs.readFile(packagePath);
    let package = JSON.parse(packageJSON.toString());

    let newVer = sv.inc(package.version, release);

    package.version = newVer;

    fs.writeFile(packagePath, JSON.stringify(package, null, 4));

}

updatePackageJSON();