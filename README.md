# New
- Added a new GlideScopedEvaluate with try/catch functionality snippet `yalaGSE`
- Added `yalaTrace` and `yalaTraceGeneral` for the new .trace() function in SystemLogHelper
- Added `yalaStringify` and `yalaStringifyGeneral` for the new .stringify() function in SystemLogHelper
# Updates
- Added JSDoc to `YansaClassInitFunc`
- Updated tab stops for logging lines to have $0 tag for your cursor when "tabbing out/end" of snippet
