import * as vscode from 'vscode';

//When an activation event is trigger for the first time as defined in the package.json
export function activate(context: vscode.ExtensionContext) {

	//Just snippets at this point, so we'll just let them know it got activated!
	//vscode.window.showInformationMessage('Yansa Internal Code Helper Activated. Enjoy!');
}

//when a deactivation event occurs.
export function deactivate() { }
